Author: http://drupal.org/user/183191


This is a sandbox project, which contains experimental code for developer use only.

List all defined feed sources:
$ drush flist

Import a source (using HTTP fetching only):
$ drush fim my_article_node_importer http://example.org/xml/articles.xml

Delete feed items from a given source:
$ drush fdel my_article_node_importer
