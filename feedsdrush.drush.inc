<?php

/**
 * Implements hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function feedsdrush_drush_command() {
  $items = array();

  $items['feedsdrush-list-feed-ids'] = array(
    'description' => "List feed ids.",
    'aliases' => array("flist"),
  );

  $items['feedsdrush-import-feed'] = array(
    'description' => "Fetch the given feed.",
    'aliases' => array("fim"),
    'examples' => array(
      'drush fim my_article_node_importer http://example.org/xml/articles.xml' => 'Imports the defined my_article_node_importer using the given URL.',
      'drush fim my_article_node_importer' => 'Imports the defined my_article_node_importer using the predefined URL if found.',
    ),
  );

  $items['feedsdrush-delete-feed-items'] = array(
    'description' => "Delete the imported items of the given feed.",
    'aliases' => array('fdel'),
    'examples' => array(
      'drush fdel my_article_node_importer' => 'Delete all feed items from the given source.',
    ),
  );

  $items['feedsdrush-reset-feed-items'] = array( 
    'description' => "Reset feed item hashes",
    'aliases' => array('freset'),
    'examples' => array( 
        'drush freset my_article_node_importer' => 'Reset all hashes for feed items from the given source.',
    ),
  );
  return $items;
}

/**
 * Display a list of available feed sources.
 * Also the disabled are included in the list.
 */
function drush_feedsdrush_list_feed_ids() {
  if ($importers = feeds_importer_load_all()) {
    $rows = array();
    foreach ($importers as $importer) {
      $status = ($importer->disabled)? '[disabled]' : '';
      $id = $importer->id;
      $name = $importer->config['name'];
      $row = array($id, $name, $status);
      $rows[] = $row;
    }
    drush_print_table($rows, FALSE, array());
  }
}

/**
 * Delete all feed items from the given source.
 */
function drush_feedsdrush_delete_feed_items($importer_id = '') {
  $source = feeds_source($importer_id);
  if ($source) {
    while (FEEDS_BATCH_COMPLETE != $source->clear());
  }
}

/**
 * Import the given source.
 */
function drush_feedsdrush_import_feed($importer_id = '', $url = '') {
  $source = feeds_source($importer_id);

  // Override source URL manually
  if (!empty($url)) {
    $config = array(
      'FeedsHTTPFetcher' => array('source' => $url)
    );
    $source->addConfig($config);
    $source->save();
  }
  else {
    $url = $source->config['FeedsHTTPFetcher']['source'];
  }

  if (empty($url)) {
    // Predefined URL cannot be found.
    // Processing cannot continue.
    drush_print('Source URL must be given.');
  }
  else {
    while (FEEDS_BATCH_COMPLETE != $source->import());
  }

}

function drush_feedsdrush_reset_feed_items($importer_id = '') {
  $importers = feeds_importer_load_all( );
  foreach ($importers as $importer) {
    $options[] = $importer->id;
  }
  if (!empty($importer_id) && in_array($importer_id, $options)) {
    _drush_feedsdrush_reset_feed_items($importer_id);
  } else {
    $selection = drush_choice($options, dt('Select feed source'));
    _drush_feedsdrush_reset_feed_items($options[$selection]);
  }
}

function _drush_feedsdrush_reset_feed_items($importer_id){
  $affected = db_update('feeds_item')
            ->fields(array('hash' => 'd41d8cd98f00b204e9800998ecf8427e'))
            ->condition('id', $importer_id, 'LIKE')
            ->execute();
  drush_print('Reseted '. $affected . ' items');
}

